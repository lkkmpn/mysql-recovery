# mysql-recovery

A tool to recover MySQL databases from .ibd files (InnoDB storage engine). This
tool has been developed for recovery of a MySQL 8 database, but has not been
tested extensively and is missing a number of implementation details—use at
your own risk! The codebase and approach of this tool is heavily based on the
[mysql_ibd](https://github.com/anyongjin/mysql_ibd) project.

## Installation

Clone this project and install the package with `pip` (optionally in a virtual
environment).

```
$ git clone git@gitlab.com:lkkmpn/mysql-recovery.git
$ cd mysql-recovery
$ python3 -m pip install .
```

## Usage

Create a configuration file defining the databases to recover (their names and
directories containing .ibd files for their tables), and the database
connection to recover them to; see [`config-sample.json`](config-sample.json)
for an example configuration file. Then, run the recovery tool from the command
line, providing the configuration file path as a parameter:

```
$ mysql-recovery config.json
```

For the best results, recover into a MySQL installation with a version close to
the version the original databases were running in.

Note that you need to run the tool as a user with permissions to change
ownership and create files in the MySQL installation directory. This might mean
running as `root`, which you can do as follows if you installed the package
into an activated virtual environment:

```
$ sudo -E env PATH=$PATH mysql-recovery config.json
```

## License

This project is licensed under [the MIT license.](LICENSE.md)
