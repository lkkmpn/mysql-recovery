from setuptools import find_packages, setup

setup(
    name='mysql-recovery',
    version='0.0.1',
    description='Recover MySQL databases from .ibd files',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'PyMySQL'
    ],
    entry_points={
        'console_scripts': [
            'mysql-recovery = mysql_recovery.__main__:main'
        ]
    }
)
