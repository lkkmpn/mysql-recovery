from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Optional


@dataclass
class Database:
    name: str
    ibd_directory: str
    mysql_directory: Optional[str] = None

    @property
    def ibd_path(self) -> Path:
        return Path(self.ibd_directory)

    @property
    def mysql_installation_directory(self) -> str:
        return self.mysql_directory if self.mysql_directory is not None else self.name


Databases = List[Database]


@dataclass
class MySQLConnection:
    host: str
    port: int
    user: str
    password: str
    charset: str
    connect_timeout: int
    read_timeout: int


@dataclass
class MySQLInstallation:
    directory: str
    user: str
    group: str

    @property
    def path(self) -> Path:
        return Path(self.directory)


@dataclass
class MySQL:
    connection: MySQLConnection
    installation: MySQLInstallation


@dataclass
class Config:
    databases: Databases
    mysql: MySQL


def create_config(config_dict: Dict[str, Any]) -> Config:
    databases: List[Database] = [Database(**database) for database in config_dict.get('databases')]
    mysql_connection = MySQLConnection(**config_dict.get('mysql').get('connection'))
    mysql_installation = MySQLInstallation(**config_dict.get('mysql').get('installation'))
    mysql = MySQL(mysql_connection, mysql_installation)
    config = Config(databases, mysql)
    return config
