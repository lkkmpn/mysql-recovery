import argparse
import json
from pathlib import Path

from .config_data import create_config
from .log import setup_logger
from .recovery import Recovery


def main():
    parser = argparse.ArgumentParser(prog='mysql-recovery',
                                     description='Recover MySQL databases from .ibd files')

    parser.add_argument('config_path', help='location of configuration file')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='more verbose log output')

    args = parser.parse_args()
    config_path = Path(args.config_path)
    setup_logger(args.verbose)

    with open(config_path, 'r') as f:
        config_dict = json.load(f)

    config = create_config(config_dict)

    Recovery(config).run()


if __name__ == '__main__':
    main()
