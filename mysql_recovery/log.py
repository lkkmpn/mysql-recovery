import logging


def setup_logger(verbose: bool) -> None:
    """Set up the logger.

    Parameters
    ----------
    verbose : bool
        Whether to enable verbose logging (DEBUG-level statements).
    """
    logger = logging.getLogger(__name__)

    if logger.hasHandlers():
        return

    logger_level = logging.DEBUG if verbose else logging.INFO

    stream_formatter = logging.Formatter(fmt='%(asctime)s %(levelname)s %(message)s')
    ch = logging.StreamHandler()
    ch.setLevel(logger_level)
    ch.setFormatter(stream_formatter)

    logger.addHandler(ch)
    logger.setLevel(logger_level)


def get_logger() -> logging.Logger:
    """Return the logger.

    Returns
    -------
    logging.Logger
        Logger object.
    """
    return logging.getLogger(__name__)
