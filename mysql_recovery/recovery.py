import json
import shutil
import subprocess
from pathlib import Path
from typing import Dict, List, Optional

import pymysql

from .config_data import Config, Database
from .log import get_logger


class Recovery:
    def __init__(self, config: Config):
        """Create an object for performing a MySQL recovery.

        Parameters
        ----------
        config : Config
            Configuration object.

        Raises
        ------
        FileNotFoundError
            Raised when the `ibd2sdi` executable cannot be found.
        """
        self.config = config

        ibd2sdi_path = shutil.which('ibd2sdi')
        if ibd2sdi_path is None:
            raise FileNotFoundError('`ibd2sdi` not found!')

        self.ibd2sdi = Path(ibd2sdi_path)

        self.tmp_path = Path('tmp-mysql-recovery')
        if self.tmp_path.exists():
            shutil.rmtree(self.tmp_path)
        self.tmp_path.mkdir()

        self.logger = get_logger()

    def run(self):
        """Perform the MySQL recovery.
        """
        for database in self.config.databases:
            self.logger.info(f'Database: {database.name}')

            self.ibd_to_sdi(database)
            if self.create_database(database):
                table_names = self.sdi_to_tables(database)
                self.replace_tables(database, table_names)

        shutil.rmtree(self.tmp_path)

    def ibd_to_sdi(self, database: Database):
        """Create .sdi files for every table in a database using the `ibd2sdi`
        tool, based on the .ibd file backups.

        See https://dev.mysql.com/doc/refman/8.0/en/serialized-dictionary-information.html.

        Parameters
        ----------
        database : Database
            Database configuration information.
        """
        self.logger.info('Step 1: .ibd to .sdi')

        sdi_dir = self.tmp_path / 'sdi' / database.name
        sdi_dir.mkdir(parents=True)

        ibd_paths = list(database.ibd_path.glob('*.ibd'))
        for ibd_path in ibd_paths:
            ibd_name = ibd_path.stem
            self.logger.debug(f'Table: {ibd_name}')

            sdi_path = sdi_dir / f'{ibd_name}.sdi'
            p = subprocess.run([self.ibd2sdi, f'--dump-file={sdi_path}', ibd_path],
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if p.returncode != 0:
                self.logger.warning(f'{ibd_name}: `ibd2sdi` did not succeed, skipping this table. Stdout:\n' +
                                    p.stdout.decode('utf-8').strip() +
                                    '\nStderr:\n' +
                                    p.stderr.decode('utf-8').strip())
                sdi_path.unlink(missing_ok=True)
                continue

    def create_database(self, database: Database) -> bool:
        """Create a new database on the destination MySQL server.

        Parameters
        ----------
        database : Database
            Database configuration information.

        Returns
        -------
        bool
            True if the database was created successfully.
        """
        self.logger.info('Step 2: Create database')

        connection = self.mysql_connection(None)
        if not connection:
            return
        cursor = connection.cursor()

        try:
            cursor.execute(f'CREATE DATABASE `{database.name}`;')

            success = True
        except pymysql.err.OperationalError as e:
            self.logger.warning(f'MySQL error when creating database {database.name}: {e.args[1]}. '
                                'Skipping this database.')

            success = False

        connection.close()

        return success

    def sdi_to_tables(self, database: Database) -> List[str]:
        """Convert the serialized dictionary information from the .sdi files
        into CREATE TABLE statements, and execute these on the destination
        MySQL server to create the respective tables.

        Parameters
        ----------
        database : Database
            Database configuration information.

        Returns
        -------
        List[str]
            List of successfully created tables.
        """
        self.logger.info('Step 3: .sdi to CREATE TABLE')

        table_names: List[str] = []

        sdi_dir = self.tmp_path / 'sdi' / database.name
        sdi_paths = list(sdi_dir.glob('*.sdi'))
        for sdi_path in sdi_paths:
            # recreate the connection for every table to minimize the chance of connection timeouts
            connection = self.mysql_connection(database)
            if not connection:
                return
            cursor = connection.cursor()

            sdi_name = sdi_path.stem
            self.logger.debug(f'Table: {sdi_name}')

            with open(sdi_path, 'r', encoding='utf-8') as f:
                sdi_data: Dict = json.load(f)[1]['object']

            sdi_object_type = sdi_data.get('dd_object_type')
            if sdi_object_type != 'Table':
                self.logger.warning(f'Unsupported sdi object type `{sdi_object_type}`, skipping this table.')
                continue

            dd_object: Dict = sdi_data.get('dd_object')
            columns: List[Dict] = dd_object.get('columns')
            indexes: List[Dict] = dd_object.get('indexes')
            table_name: str = dd_object.get('name')
            table_engine: str = dd_object.get('engine')

            sql = f'CREATE TABLE `{table_name}` (\n'

            for i, column in enumerate(columns):
                if column.get('hidden') == 2:
                    continue
                if i > 0:
                    sql += ',\n'

                column_name = column.get('name')
                column_type = column.get('column_type_utf8')

                sql += f'\t`{column_name}` {column_type}'

                if not column.get('is_nullable'):
                    sql += ' NOT NULL'
                if column.get('is_auto_increment'):
                    sql += ' AUTO_INCREMENT'
                if not column.get('has_no_default'):
                    if column.get('default_value_null'):
                        sql += ' DEFAULT NULL'
                    else:
                        default_value = column.get('default_value_utf8')
                        if default_value:
                            # we only handle `CURRENT_TIMESTAMP` as expression default
                            if default_value == 'CURRENT_TIMESTAMP':
                                sql += f' DEFAULT {default_value}'
                            else:
                                sql += f' DEFAULT \'{default_value}\''

                comment = column.get('comment')
                if comment:
                    sql += f' COMMENT {comment}'

            for index in indexes:
                if index.get('hidden'):
                    continue

                elements = index.get('elements')
                if not elements:
                    continue

                index_name = index.get('name')

                elements = [element for element in elements if element['length'] < 4294967295]
                if not elements:
                    self.logger.warning(f'{sdi_name}: {index_name} has no linked columns, skipping')
                    continue

                sql += ',\n\t'

                column_names = [columns[element.get('column_opx')].get('name') for element in elements]
                columns_str = ', '.join(f'`{name}`' for name in column_names)

                index_type = index.get('type')
                if index_type == 1:
                    sql += f'PRIMARY KEY ({columns_str})'
                elif index_type in {2, 3}:
                    sql += f'INDEX `{index_name}` ({columns_str})'
                else:
                    self.logger.warning(f'{sdi_name}: Unsupported index type {index_type} for {index_name}, skipping')
                    continue

            sql += f'\n) ENGINE={table_engine};'

            self.logger.debug('SQL to create the table:\n' + sql)

            cursor.execute(f'DROP TABLE IF EXISTS `{table_name}`;')
            try:
                cursor.execute(sql)
            except pymysql.err.MySQLError as e:
                self.logger.warning(f'MySQL error when creating table {table_name}: {e.args[1]}. '
                                    'Deleting this table and skipping it; check it manually.')

                cursor.execute(f'DROP TABLE IF EXISTS `{table_name}`;')
            else:
                table_names.append(sdi_name)

            connection.close()

        return table_names

    def replace_tables(self, database: Database, table_names: List[str]):
        """Replace the MySQL table files with the .ibd files from the backup,
        and re-import their tablespace.

        Parameters
        ----------
        database : Database
            Database configuration information.
        table_names : List[str]
            List of tables to replace.
        """
        self.logger.info('Step 4: Replacing tables')

        installation_config = self.config.mysql.installation

        connection = self.mysql_connection(database)
        if not connection:
            return
        cursor = connection.cursor()

        for table_name in table_names:
            ibd_path = database.ibd_path / f'{table_name}.ibd'
            ibd_size = ibd_path.stat().st_size

            self.logger.debug(f'Table: {table_name} (size: {ibd_size})')

            destination_path = Path(installation_config.directory) / \
                database.mysql_installation_directory / f'{table_name}.ibd'

            # create a temporary copy of the empty table in case importing the tablespace fails
            temp_copy_path = self.tmp_path / f'{table_name}-empty.ibd'
            shutil.copy(destination_path, temp_copy_path)

            cursor.execute(f'ALTER TABLE `{table_name}` DISCARD TABLESPACE;')

            shutil.copy(ibd_path, destination_path)
            shutil.chown(destination_path, user=installation_config.user, group=installation_config.group)

            try:
                cursor.execute(f'ALTER TABLE `{table_name}` IMPORT TABLESPACE;')
            except pymysql.err.MySQLError as e:
                self.logger.warning(f'MySQL error when importing table {table_name}: {e.args[1]}. '
                                    'Restoring this table to an empty state and skipping it; check it manually.')

                shutil.copy(temp_copy_path, destination_path)
                shutil.chown(destination_path, user=installation_config.user, group=installation_config.group)
                cursor.execute(f'ALTER TABLE `{table_name}` IMPORT TABLESPACE;')

            temp_copy_path.unlink(missing_ok=True)

        connection.close()

    def mysql_connection(self, database: Optional[Database]) -> Optional[pymysql.connections.Connection]:
        """Create a connection to the MySQL server.

        Parameters
        ----------
        database : Optional[Database]
            Database to connect to, or None to not connect to a specific
            database.

        Returns
        -------
        Optional[pymysql.connections.Connection]
            MySQL connection, or None if it could not be created.
        """
        config = self.config.mysql.connection

        try:
            connection = pymysql.connect(
                host=config.host,
                port=config.port,
                user=config.user,
                password=config.password,
                database=database.name if database else None,
                charset=config.charset,
                connect_timeout=config.connect_timeout,
                read_timeout=config.read_timeout
            )
        except pymysql.err.OperationalError as e:
            self.logger.error(
                f'MySQL error connecting to the MySQL server: {e.args[1]}. Please check the configuration file.')
        else:
            return connection
